$(() => {

  // ready for Javascript which will run when the html is loaded
  /*const faces = $('faces');
  const add = $('<b>').html('Hello');
  faces.append(add);*/

  const faces = $('faces');
  //const name = "Thiru";
  const makeFace = function(name) {
    return $("<img>", {
      title:name,
      src:`img/2018/${name}.jpg`,
      class:"face"
    });
  };
  const names = ["Aishu",
  "Akhil",
  "Akshat",
  "Ameya",
  "Anushree",
  "Gayatri",
  "Janani",
  "John",
  "Jon",
  "Karthika",
  "Keerthana",
  "Mahidher",
  "Mariah",
  "Pavithrann",
  "Rishi",
  "RVishnuPriya",
  "Sanjana",
  "Santhosh",
  "Shruti",
  "Sindhu",
  "Sudeep",
  "Thamizh",
  "Varsha",
  "Vishnu",
  "Thiru",
"Supriya"];
  names.forEach(function(name) {
  faces.append(makeFace(name));
})
});
