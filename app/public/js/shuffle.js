'use strict';

module.exports = {
  randomInt,
  randomshuffle
};

function randomInt(hi) {
 return Math.floor(Math.random()*(hi-1));
}

function randomshuffle(shuffle) {
  let x = shuffle.length;
  for(let i = 0; i < x; i++) {
    shuffle[i] = shuffle.splice(randomInt(x),1,shuffle[i])[0];
  }
  return shuffle;
}
